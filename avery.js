require('./src/util/Extensions')

const CustomClient = require('./src/struct/CustomClient')
const Log = require('./src/util/Log')

const client = new CustomClient({
  token: process.env.token,
  owner: JSON.parse(process.env.owner),
  prefix: process.env.prefix
}).build()

client
  .on('disconnect', () => Log.warn('Conexão perdida...'))
  .on('reconnect', () => Log.info('Tentando reconectar...'))
  .on('error', err => Log.error(err))
  .on('warn', info => Log.warn(info))
  .on('debug', dbg => Log.debug(dbg))
  .on('ready', () => Log.info('Client is ready!', {tag: 'Client'}))

client.start()

process.on('unhandledRejection', err => {
  Log.error('Um erro desconhecido ocorreu!')
  Log.stacktrace(err)
})
