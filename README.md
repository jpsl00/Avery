# Avery

Um bot brasileiro customizável com algumas funções de moderação **(WIP)**

## Comandos
- Administração
  - Blacklist **(?blacklist)**
  - Prefix **(?prefix)**
- Experimental
  - Help **(?help)**
- Games
  - Rocket League
    - Stats **(?rlstats)**
    - Rank **(?rlrank)**
- Moderação
  - Ban **(?ban)**
  - Kick **(?kick)**
  - Purge **(?purge)**
- Utilitários
  - Ping **(?ping)**
  - Informação 
    - User Info **(?userinfo)**

[![Discord Bots](https://discordbots.org/api/widget/359128300519555072.svg)](https://discordbots.org/bot/359128300519555072)

**Para sugerir uma função abra uma [issue](https://github.com/jpsl00/AveryBot/issues)!**