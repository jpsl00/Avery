const path = require('path')
const readdir = require('util').promisify(require('fs').readdir)
const Sequelize = require('sequelize')

// const { dbURL } = require('../../config.json')
const Log = require('../util/Log')

const db = new Sequelize(process.env.DATABASE_URL, {
  logging: false,
  operatorsAliases: Sequelize.Op,
  dialectOptions: {
    ssl: true
  }
})

class Database {
  static get db () {
    return db
  }

  static async auth () {
    try {
      await db.authenticate()
      Log.info('Conexão com a base de dados bem-sucedida.', { tag: 'PostgreSQL' })
      await this.loadModels(path.join(__dirname, '..', 'models'))
    } catch (err) {
      Log.error('Incapaz de conectar-se a base de dados:', { tag: 'PostgreSQL' })
      Log.stacktrace(err, { tag: 'PostgreSQL' })
      Log.info('Tentando novamente em 5 segundos...', { tag: 'PostgreSQL' })
      setTimeout(() => { this.auth() }, 5000)
    }
  }

  static async loadModels (modelsPath) {
    let files = await readdir(modelsPath)

    for (let file of files) {
      let filePath = path.join(modelsPath, file)
      if (!filePath.endsWith('.js')) continue
      await require(filePath).sync({ alter: true }) // eslint-disable-line no-await-in-loop
    }
  }
}

module.exports = Database
