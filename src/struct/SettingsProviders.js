const { SequelizeProvider } = require('discord-akairo')
const { Guild } = require('discord.js')

class SettingsProvider extends SequelizeProvider {
  constructor (table) {
    super(table, {
      idColumn: 'guildID',
      dataColumn: 'settings'
    })
  }

  get (guild, key, defaultValue) {
    const id = this.constructor.getGuildID(guild)
    return super.get(id, key, defaultValue)
  }

  set (guild, key, value) {
    const id = this.constructor.getGuildID(guild)
    return super.set(id, key, value)
  }

  delete (guild, key) {
    const id = this.constructor.getGuildID(guild)
    return super.delete(id, key)
  }

  clear (guild) {
    const id = this.constructor.getGuildID(guild)
    return super.clear(id)
  }

  static getGuildID (guild) {
    if (guild instanceof Guild) return guild.id
    if (guild === 'global' || guild === null) return 'global'
    if (typeof guild === 'string' && /^\d+$/.test(guild)) return guild
    throw new TypeError('Guilda inválida especificada. Pode ser a instância de Guilda, ID da guilda, "global", ou null.')
  }
}

module.exports = SettingsProvider
