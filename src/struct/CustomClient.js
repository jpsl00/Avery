const path = require('path')
const { AkairoClient } = require('discord-akairo')

const db = require('./Database')
const SettingsProvider = require('./SettingsProviders')
const Setting = require('../models/settings')
const Log = require('../util/Log')
// const i18n = require('../util/i18n')

const DBL = require('dblapi.js')

const { MessageEmbed } = require('discord.js')

class CustomClient extends AkairoClient {
  constructor (config) {
    super({
      aliasReplacement: /-/g,
      prefix: message => this.settings.get(message.guild, 'prefix', config.prefix),
      allowMention: true,
      handleEdits: true,
      commandUtilLifetime: 300000,
      ownerID: config.owner,
      commandDirectory: path.join(__dirname, '..', 'commands'),
      inhibitorDirectory: path.join(__dirname, '..', 'inhibitors'),
      listenerDirectory: path.join(__dirname, '..', 'listeners'),
      automateCategories: true,
      defaultCooldown: 5000,
      defaultPrompt: {
        modifyStart: (text, msg) => {
          let embed = new MessageEmbed()
            .setDescription(`${text}`)
            .setColor(`${msg.member.displayHexColor === '#000000' ? msg.guild.me.displayHexColor : msg.member.displayHexColor}`)
            .setFooter(`Digite cancelar para cancelar o comando`, `${msg.author.displayAvatarURL()}`)
          return embed
        },
        modifyRetry: (text, msg) => {
          let embed = new MessageEmbed()
            .setDescription(`${text}`)
            .setColor(`${msg.member.displayHexColor === '#000000' ? msg.guild.me.displayHexColor : msg.member.displayHexColor}`)
            .setFooter(`Digite cancelar para cancelar o comando`, `${msg.author.displayAvatarURL()}`)
          return embed
        },
        timeout: msg => {
          let embed = new MessageEmbed()
            .setDescription(`**Tempo esgotado**`)
            .setColor(`${msg.member.displayHexColor === '#000000' ? msg.guild.me.displayHexColor : msg.member.displayHexColor}`)
            .setFooter(`Comando cancelado`, `${msg.author.displayAvatarURL()}`)
          return embed
        },
        ended: msg => {
          let embed = new MessageEmbed()
            .setDescription(`**Muitas tentativas**`)
            .setColor(`${msg.member.displayHexColor === '#000000' ? msg.guild.me.displayHexColor : msg.member.displayHexColor}`)
            .setFooter(`Comando cancelado`, `${msg.author.displayAvatarURL()}`)
          return embed
        },
        cancel: msg => {
          let embed = new MessageEmbed()
            .setDescription(`**Comando cancelado pelo usuário**`)
            .setColor(`${msg.member.displayHexColor === '#000000' ? msg.guild.me.displayHexColor : msg.member.displayHexColor}`)
            .setFooter(`Comando cancelado`, `${msg.author.displayAvatarURL()}`)
          return embed
        },
        cancelWord: `cancelar`,
        retries: 2,
        time: 15000
      }
    }, {
      disableEveryone: true,
      disabledEvents: ['TYPING_START', 'TYPING_STOP']
    })

    // this.localization = new i18n() // eslint-disable-line new-cap
    this.dbl = new DBL(process.env.dblKey, this)
    this.config = config
    this.settings = new SettingsProvider(Setting)
  }

  async start () {
    Log.info('Client is starting...', {tag: 'Client'})
    await db.auth()
    await this.settings.init()
    return this.login(this.config.token)
  }

  static log (channel, { embed }) {
    if (!this.client.uptime) return
    this.client.channels.get(channel).send({ embed })
  }
}

module.exports = CustomClient
