const { Inhibitor } = require('discord-akairo')

class BlacklistInhibitor extends Inhibitor {
  constructor () {
    super('blacklist', { reason: 'blacklist' })
  }

  exec (message, command) {
    const blacklist = this.client.settings.get(message.guild, 'blacklist', [])
    if (!blacklist.includes(message.author.id) || this.client.ownerID.includes(message.author.id)) return false

    if (message.channel.permissionsFor(this.client.user).has('SEND_MESSAGES')) {
      message.util.reply(`Você está na blacklist desse servidor (**${message.guild.name}**).`)
    } else {
      message.author.send(`Você está na blacklist do servidor **${message.guild.name}**`)
    }
    return true
  }
}

module.exports = BlacklistInhibitor
