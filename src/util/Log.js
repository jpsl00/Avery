const colors = require('colors')
const moment = require('moment')
const util = require('util')
const hook = require('./Webhook')

class Log {
  static log (content, { color = 'grey', tag = 'Log' } = {}) {
    this.write(content, { color, tag })
  }

  static info (content, { color = 'green', tag = 'Info' } = {}) {
    this.write(content, { color, tag })
  }

  static debug (content, { color = 'magenta', tag = 'Debug' } = {}) {
    if (process.env.debugMode === 'true') {
      this.write(content, { color, tag })
    }
  }

  static warn (content, { color = 'yellow', tag = 'Warn' } = {}) {
    this.write(content, { color, tag })
  }

  static error (content, { color = 'red', tag = 'Error' } = {}) {
    this.write(content, { color, tag, error: true })
  }

  static stacktrace (content, { color = 'white', tag = 'Error' } = {}) {
    this.write(content, { color, tag, error: true })
  }

  static write (content, { color = 'grey', tag = 'Log', error = false } = {}) {
    const timestamp = colors.cyan(`[${moment().format('DD/MM/YYYY HH:mm:ss')}]:`)
    const levelTag = colors.bold(`[${tag}]:`)
    const text = colors[color](this.clean(content))
    const stream = error ? process.stderr : process.stdout
    stream.write(`${timestamp} ${levelTag} ${text}\n`)
    hook.send(`**${timestamp}** *${levelTag}* ${error ? `\n\`\`\`js\n${text}\`\`\`` : `\`${text}\``}`)
    if (error) {
      /* Sends to Error Webhook */
      hook.send(`**${timestamp}** *${levelTag}* \n\`\`\`js\n${text}\`\`\``, { hookType: 'error' })
    }
  }

  static clean (item) {
    if (typeof item === 'string') {
      return item
        .replace(/`/g, `\`${String.fromCharCode(8203)}`)
        .replace(/@/g, `@${String.fromCharCode(8203)}`)
        .replace(/[\w\d]{24}\.[\w\d]{6}\.[\w\d-_]{27}/g, '[TOKEN]')
        .replace(/email: '[^']+'/g, `email: '[EMAIL]'`)
    }
    const cleaned = util.inspect(item, { depth: Infinity })
    return cleaned
  }
}

module.exports = Log
