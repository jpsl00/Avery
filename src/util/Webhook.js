const request = require('snekfetch')
const { dfltUsername, dfltAvatarURL } = JSON.parse(process.env.hookInfo)

class Webhook {
  static getHook (type) {
    const webhooks = JSON.parse(process.env.webhooks)
    if (!Object.keys(webhooks).includes(type)) throw new Error(`A webhook definida como ${type} não foi encontrada na declaração de variáveis de ambiente!`)
    return webhooks[type]
  }

  static send (content, { hookType = 'console', username = dfltUsername, avatarURL = dfltAvatarURL } = {}) {
    const webhookURL = this.getHook(hookType)
    request.post(webhookURL, {
      data: {
        username: username,
        avatar_url: avatarURL,
        content: content
      }
    }).catch(e => {})
  }
}

module.exports = Webhook
