// Export some useful things
module.exports = {
  emoji: {
    enabled: {
      true: '✅',
      false: '❌'
    }
  },
  text: {
    enabled: {
      true: 'Habilitado',
      false: 'Desabilitado'
    },
    optional: {
      true: '[]',
      false: '<>',
      undefined: '<>'
    }
  }
}
