const { CommandUtil } = require('discord-akairo')
const { Message, MessageEmbed } = require('discord.js')

Object.defineProperty(CommandUtil.prototype, 'reply', {
  value: function reply (content, options) {
    return this.send(`${this.message.author} **≡** ${Array.isArray(content) ? content.join('\n') : content}`, options)
  }
})

Object.defineProperty(Message.prototype, 'reply', {
  value: function reply (content, options) {
    return this.channel.send(`${this.author} **≡** ${Array.isArray(content) ? content.join('\n') : content}`, options)
  }
})

Object.defineProperty(CommandUtil.prototype, 'sendGuild', {
  value: function send (content, options) {
    if (this.message.channel.type === 'text') return this.message.channel.send(content)
  }
})

Object.defineProperty(Message.prototype, 'sendGuild', {
  value: function send (content, options) {
    if (this.channel.type === 'text') return this.channel.send(content)
  }
})

Object.defineProperty(CommandUtil.prototype, 'replyEmbed', {
  value: function reply (content, options) {
    let embed = new MessageEmbed()
      .setDescription(content)
      .setColor(`${this.message.guild ? this.message.member.displayHexColor === '#000000' ? this.message.guild.me.displayHexColor : this.message.member.displayHexColor : '#ff5722'}`)
      .setFooter(`${this.message.author.username}`, `${this.message.author.displayAvatarURL()}`)
    return this.send({ embed }, options)
  }
})

Object.defineProperty(Message.prototype, 'replyEmbed', {
  value: function reply (content, options) {
    let embed = new MessageEmbed()
      .setDescription(content)
      .setColor(`${this.guild ? this.member.displayHexColor === '#000000' ? this.guild.me.displayHexColor : this.member.displayHexColor : '#ff5722'}`)
      .setFooter(`${this.author.username}`, `${this.author.displayAvatarURL()}`)
    return this.channel.send({ embed }, options)
  }
})

/* eslint no-extend-native:0 */

Object.defineProperty(String.prototype, 'toTitleCase', {
  value: function (onlyFirst) {
    const first = this.charAt(0).toUpperCase()
    const rest = (onlyFirst) ? this.substr(1) : this.substr(1).toLowerCase()
    return first + rest
  }
})

Object.defineProperty(String.prototype, 'toTitleCaseAll', {
  value: function (delim, onlyFirst) {
    const d = delim || ' '
    return this.split(d).reduce((acc, cur) => {
      if (acc === '') return cur.toTitleCase(onlyFirst)
      return acc + d + cur.toTitleCase(onlyFirst)
    }, '')
  }
})
