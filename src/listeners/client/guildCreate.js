const { Listener } = require('discord-akairo')
const Log = require('../../util/Log')

class GuildCreateListener extends Listener {
  constructor () {
    super('guildCreate', {
      event: 'guildCreate',
      emitter: 'client',
      category: 'client'
    })
  }

  exec (guild) {
    Log.info(`Joined guild: ${guild.name} (${guild.id})`, { tag: 'Client' })
  }
}

module.exports = GuildCreateListener
