const { Listener } = require('discord-akairo')

class ReadyListener extends Listener {
  constructor () {
    super('ready', {
      event: 'ready',
      emitter: 'client',
      category: 'client'
    })
  }

  exec () {
    this.client.user.setPresence({
      status: 'online',
      activity: {
        name: '@Avery help',
        type: 'STREAMING',
        url: 'https://www.twitch.tv/AC_jpsl00'
      }
    })
  }
}

module.exports = ReadyListener
