const { Listener } = require('discord-akairo')
const Log = require('../../util/Log')

class CommandBlockedListener extends Listener {
  constructor () {
    super('commandBlocked', {
      event: 'commandBlocked',
      emitter: 'commandHandler',
      category: 'commands'
    })
  }

  exec (message, command, reason) {
    let text = {
      owner: () => 'Você tem que ser o dono do bot para usar este comando.',
      guild: () => 'Você tem que estar em uma guilda para usar este comando.'
    }[reason]

    let tag = message.guild ? message.guild.name : `${message.author.tag}/PM`
    Log.log(`=> ${command.id} ~ Blocked (${reason})`, { tag })

    if (!text) return
    if (message.guild ? message.channel.permissionsFor(this.client.user).has('SEND_MESSAGES') : true) {
      message.util.reply(text())
    }
  }
}

module.exports = CommandBlockedListener
