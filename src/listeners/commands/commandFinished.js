const { Listener } = require('discord-akairo')
const Log = require('../../util/Log')

class CommandFinishedListener extends Listener {
  constructor () {
    super('commandFinished', {
      event: 'commandFinished',
      emitter: 'commandHandler',
      category: 'commands'
    })
  }

  exec (message, command, args, returnValue) {
    let tag = message.guild ? message.guild.name : `${message.author.tag}/PM`
    Log.log(`=> ${command.id} ~ Execution finished`, { tag })
  }
}

module.exports = CommandFinishedListener
