const { Listener } = require('discord-akairo')
const Log = require('../../util/Log')

class CooldownListener extends Listener {
  constructor () {
    super('cooldown', {
      event: 'cooldown',
      emitter: 'commandHandler',
      category: 'commands'
    })
  }

  exec (message, command, remaining) {
    let text = `O comando ${command.id} está em cooldown. Tente novamente em ${remaining / 1000}s`

    let tag = message.guild ? message.guild.name : `${message.author.tag}/PM`
    Log.log(`=> ${command.id} ~ Cooldown (+${remaining / 1000}s)`, { tag })

    if (message.guild ? message.channel.permissionsFor(this.client.user).has('SEND_MESSAGES') : true) {
      message.util.reply(text)
    }
  }
}

module.exports = CooldownListener
