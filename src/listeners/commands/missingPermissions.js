const { Listener } = require('discord-akairo')
const Log = require('../../util/Log')

class MissingPermissionsListener extends Listener {
  constructor () {
    super('missingPermissions', {
      event: 'missingPermissions',
      emitter: 'commandHandler',
      category: 'commands'
    })
  }

  exec (message, command, type, missing) {
    let perm = missing.map(e => e.replace(/_/g, ' ').toLowerCase().replace(/\b\w/g, l => l.toUpperCase())).join(', ')

    let text = {
      client: () => `Eu preciso ${missing.length > 1 ? 'das permissões' : 'da permissão'} \`${perm}\` para executar esse comando`,
      user: () => `Você precisa ${missing.length > 1 ? 'das permissões' : 'da permissão'} \`${perm}\` para executar esse comando`
    }[type]

    let tag = message.guild ? message.guild.name : `${message.author.tag}/PM`
    Log.log(`=> ${command.id} ~ Missing ${perm}`, { tag })

    if (message.guild ? message.channel.permissionsFor(this.client.user).has('SEND_MESSAGES') : true) {
      message.util.reply(text())
    }
  }
}

module.exports = MissingPermissionsListener
