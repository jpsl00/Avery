const { Command } = require('discord-akairo')

class PrefixCommand extends Command {
  constructor () {
    super('prefix', {
      aliases: ['prefixo', 'prefix'],
      description: 'Configurar um prefixo para o servidor',
      // category: 'serverManagement',
      channel: 'guild',
      userPermissions: ['MANAGE_GUILD'],
      args: [
        {
          id: 'prefixo',
          description: 'O prefixo desejado',
          type: word => {
            if (!word) return null
            if (/\s/.test(word) || word.length > 10) return null
            return word
          },
          prompt: {
            start: 'Que prefixo gostaria para esta guilda?',
            retry: 'Por favor, insira um prefixo com menos de 10 caracteres e sem espaços'
          }
        }
      ]
    })
  }

  async exec (message, { prefixo }) {
    await this.client.settings.set(message.guild, 'prefix', prefixo)
    if (prefixo === process.env.prefix) return message.util.reply(`O prefixo foi resetado ao padrão \`${process.env.prefix}\``)
    return message.util.reply(`O prefixo agora é \`${prefixo}\``)
  }
}

module.exports = PrefixCommand
