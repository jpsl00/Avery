const { Command } = require('discord-akairo')

class BlacklistCommand extends Command {
  constructor () {
    super('blacklist', {
      aliases: ['blacklist', 'unblacklist', 'bl'],
      description: 'Adicionar ou remover alguém da blacklist do servidor',
      // category: 'serverManagement',
      channel: 'guild',
      userPermissions: ['MANAGE_GUILD'],
      args: [
        {
          id: 'member',
          description: 'O membro desejado',
          match: 'content',
          type: 'member',
          prompt: {
            start: 'Que usuário deseja adicionar/remover?',
            retry: 'Por favor insira um usuário válido.'
          }
        }
      ]
    })
  }

  async exec (message, { member, server }) {
    if (message.author.id === member.id || message.guild.ownerID === member.id || this.client.ownerID.includes(member.id)) {
      return message.util.reply('Esta pessoa não pode ser adicionada na blacklist.')
    }
    const blacklist = this.client.settings.get(message.guild, 'blacklist', [])

    if (blacklist.includes(member.id)) {
      const index = blacklist.indexOf(member.id)
      blacklist.splice(index, 1)
      await this.client.settings.set(message.guild, 'blacklist', blacklist)

      return message.util.reply(`**${member.user.tag}** foi removido da blacklist.`)
    } else {
      blacklist.push(member.id)
      await this.client.settings.set(message.guild, 'blacklist', blacklist)

      return message.util.reply(`**${member.user.tag}** foi adicionado a blacklist de ${message.guild.name} e portanto não poderá usar comandos.`)
    }
  }
}

module.exports = BlacklistCommand
