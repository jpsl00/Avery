const { Command } = require('discord-akairo')

class BanCommand extends Command {
  constructor () {
    super('ban', {
      aliases: ['ban', 'banir'],
      description: 'Banir alguém',
      channel: 'guild',
      clientPermissions: ['BAN_MEMBERS'],
      userPermissions: ['BAN_MEMBERS'],
      args: [
        {
          id: 'membro',
          description: 'O membro a ser banido',
          match: 'word',
          type: 'member',
          prompt: {
            start: 'Qual membro deseja banir?',
            retry: 'Diga-me um membro válido'
          }
        },
        {
          id: 'motivo',
          description: 'O motivo do banimento',
          match: 'rest',
          type: 'string',
          prompt: {
            optional: true
          }
        }
      ]
    })
  }

  async exec (message, { membro, motivo }) {
    if (membro.bannable) {
      await membro.ban({reason: motivo})
      let zucc = await message.util.reply(`${membro.user} foi banido!`)
      return zucc.delete({timeout: 5000})
    }
    let zucc = await message.util.reply(`Não posso banir ${membro.user}`)
    return zucc.delete({timeout: 5000})
  }
}

module.exports = BanCommand
