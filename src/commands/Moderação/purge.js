const { Command } = require('discord-akairo')

class PurgeCommand extends Command {
  constructor () {
    super('purge', {
      aliases: ['purge', 'prune', 'limpar', 'deletar'],
      description: 'Deletar mensagens em massa',
      channel: 'guild',
      clientPermissions: ['MANAGE_MESSAGES'],
      userPermissions: ['MANAGE_MESSAGES'],
      args: [
        {
          id: 'mensagens',
          description: 'Número de mensagens a serem deletadas (1~100)',
          match: 'word',
          type: (word, message) => {
            if (Number.parseInt(word) >= 1 && Number.parseInt(word) <= 100) return Number.parseInt(word)
            return null
          },
          prompt: {
            start: 'Quantas mensagens deseja deletar? (1~100)',
            retry: 'Diga-me um número válido (1~100)'
          }
        }
      ]
    })
  }

  async exec (message, { mensagens }) {
    await message.delete()
    await message.channel.bulkDelete(mensagens, true)
    let zucc = await message.util.reply(`Deletei ${mensagens} mensagens 😄`)
    return zucc.delete({timeout: 5000})
  }
}

module.exports = PurgeCommand
