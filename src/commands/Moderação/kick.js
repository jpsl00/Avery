const { Command } = require('discord-akairo')

class KickCommand extends Command {
  constructor () {
    super('kick', {
      aliases: ['kick', 'expulsar'],
      description: 'Expulsar alguém',
      channel: 'guild',
      clientPermissions: ['KICK_MEMBERS'],
      userPermissions: ['KICK_MEMBERS'],
      args: [
        {
          id: 'membro',
          description: 'O membro a ser expulso',
          match: 'word',
          type: 'member',
          prompt: {
            start: 'Qual membro deseja expulsar?',
            retry: 'Diga-me um membro válido'
          }
        },
        {
          id: 'motivo',
          description: 'O motivo da expulsão',
          match: 'rest',
          type: 'string',
          prompt: {
            optional: true
          }
        }
      ]
    })
  }

  async exec (message, { membro, motivo }) {
    if (membro.kickable) {
      await membro.kick(motivo)
      let zucc = await message.util.reply(`${membro.user} foi expulso!`)
      return zucc.delete({timeout: 5000})
    }
    let zucc = await message.util.reply(`Não posso expulsar ${membro.user}`)
    return zucc.delete({timeout: 5000})
  }
}

module.exports = KickCommand
