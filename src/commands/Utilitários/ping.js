const { Command } = require('discord-akairo')

class PingCommand extends Command {
  constructor () {
    super('ping', {
      aliases: ['ping', 'pang', 'pong', 'peng', 'pung'],
      protected: true,
      cooldown: 30000,
      description: 'Pong! 🏓'
    })
  }

  async exec (message) {
    let sent = await message.util.reply('Pong!')
    let sentTime = sent.editedTimestamp || sent.createdTimestamp
    let startTime = message.editedTimestamp || message.createdTimestamp
    return message.util.reply(`Pong! \`${sentTime - startTime} ms\` Round-Trip & \`${Math.round(this.client.ping)} ms\` API`)
  }
}

module.exports = PingCommand
