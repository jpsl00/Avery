const { Command } = require('discord-akairo')
const { MessageEmbed } = require('discord.js')
const moment = require('moment')

class UserInfoCommand extends Command {
  constructor () {
    super('userInfo', {
      aliases: ['userinfo', 'ui', 'memberinfo', 'mi'],
      channel: 'guild',
      clientPermissions: ['EMBED_LINKS'],
      args: [
        {
          id: 'membro',
          description: 'Um membro do servidor',
          type: 'member',
          prompt: {
            start: '',
            retry: '',
            optional: 'true'
          },
          default: (msg) => msg.member
        }
      ],
      description: 'Ver informações sobre um membro do servidor'
    })
  }

  async exec (message, { membro }) {
    let embed = new MessageEmbed()
      .setColor(`${message.member.displayHexColor === '#000000' ? message.guild.me.displayHexColor : message.member.displayHexColor}`)
      .setAuthor(`${membro.displayName}`, `${membro.user.displayAvatarURL()}`)
      .setThumbnail(`${membro.user.displayAvatarURL()}`)
      .addField(`Username`, `${membro.user.tag}`, true)
      .addField(`ID`, `${membro.id}`, true)
      .addField(`Criado em`, `${moment(membro.user.createdTimestamp).format('HH:mm:ss DD/MM/YYYY')}`, true)
      .addField(`Entrou em`, `${moment(membro.joinedTimestamp).format('HH:mm:ss DD/MM/YYYY')}`, true)
      .addField(`Cargos`, `${membro.roles.map(r => r).join(', ')}`)
      .addField(`Permissões`, `${membro.permissions.toArray().map(p => p.replace(/_/g, ' ').toLowerCase().replace(/\b\w/g, l => l.toUpperCase())).join(', ')}`)

    return message.channel.send({ embed })
  }
}

module.exports = UserInfoCommand
