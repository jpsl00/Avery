const { Command } = require('discord-akairo')
const Log = require('../../util/Log')

class ReloadCommand extends Command {
  constructor () {
    super('reload', {
      aliases: ['reload', 'r'],
      // category: 'dono',
      ownerOnly: true,
      protected: true,
      args: [
        {
          id: 'tipo',
          match: 'prefix',
          prefix: ['tipo:', 'type:', 't:'],
          type: [['command', 'c'], ['inhibitor', 'i'], ['listener', 'l']],
          default: 'command'
        },
        {
          id: 'modulo',
          type: (word, message, { tipo }) => {
            if (!word) return null
            const resolver = this.handler.resolver.type({
              command: 'commandAlias',
              inhibitor: 'inhibitor',
              listener: 'listener'
            }[tipo])

            return resolver(word)
          }
        }
      ]
    })
  }

  exec (message, { tipo, modulo: mod }) {
    if (!mod) {
      return message.util.replyEmbed(`${tipo.toTitleCase()} ${tipo === 'command' ? 'alias' : 'ID'} inválido especificado para recarregar.`)
    }

    try {
      mod.reload()
      return message.util.replyEmbed(`${tipo.toTitleCase()} \`${mod.id}\` recarregado com sucesso.`)
    } catch (err) {
      Log.error(`Erro ao recarregar ${tipo} ${mod.id}`)
      Log.stacktrace(err)
      return message.util.replyEmbed(`Falha ao recarregar ${tipo.toTitleCase()} \`${mod.id}\`.`)
    }
  }
}

module.exports = ReloadCommand
