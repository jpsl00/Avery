const { Command } = require('discord-akairo')
const request = require('snekfetch')
const Log = require('../../../util/Log')
const { MessageEmbed } = require('discord.js')

class RocketLeagueRankCommand extends Command {
  constructor () {
    super('Rocket League Rank', {
      aliases: ['rlrank', 'rlr'],
      description: 'Ver os ranks de um jogador no Rocket League',
      typing: true,
      cooldown: 30000,
      clientPermissions: ['EMBED_LINKS'],
      args: [
        {
          id: 'platform',
          description: 'A plataforma, pode ser `Steam, PS4 ou Xbox`',
          match: 'word',
          type: (word, message) => {
            switch (word.toLowerCase()) {
              case 'computador':
              case 'steam':
              case 'pc':
                return 1
              case 'playsation 4':
              case 'playstation':
              case 'ps4':
              case 'ps':
                return 2
              case 'xbox':
              case 'xbox one':
              case 'xb1':
              case 'x1':
                return 3
              default:
                return null
            }
          },
          prompt: {
            start: 'Diga-me a plataforma desejada\n*Steam, Playstation ou Xbox*',
            retry: 'Diga-me uma plataforma válida\n*Steam, Playstation ou Xbox*'
          }
        },
        {
          id: 'player',
          description: 'O jogador, pode ser `Gamertag, SteamID64 ou URL da Steam`',
          match: 'rest',
          type: (word, message, { platform }) => {
            if (!word) { return null }
            if (platform === 2 || platform === 3) {
              word = word.length ? word : null
            }
            if (platform === 1) {
              if (/(https?:\/\/steamcommunity\.com(\/(id)?(profiles)?\/[A-Za-z0-9]*))/.test(word)) {
                word = word
                  .replace(/(https?:\/\/steamcommunity\.com(\/(id)?(profiles)?\/*))/, '')
                  .replace('/', '')
              }
              if (!/^\d+$/.test(word) || word.length !== 17) {
                return request.get(`http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/`)
                  .query('key', process.env.steamKey)
                  .query('vanityurl', word)
                  .then((r) => {
                    r = r.body.response
                    if (r.success === 42) {
                      Log.info(r.message || 'Sem mensagem', {tag: 'SteamAPIVanityParser'})
                      return null
                    }
                    return r.steamid
                  })
                  .catch((e) => {
                    Log.info(e || 'Sem mensagem', {tag: 'SteamAPIRequest'})
                    return null
                  })
              }
            }
            return word
          },
          prompt: {
            start: (message, { platform }) => {
              let placeholder
              switch (platform) {
                case 1:
                  placeholder = 'SteamID64/Url do perfil'
                  break
                case 2:
                  placeholder = 'PlaystationID'
                  break
                case 3:
                  placeholder = 'Gamertag'
                  break
              }
              return `Diga-me um(a) ${placeholder}`
            },
            retry: (message, { platform }) => {
              let placeholder
              switch (platform) {
                case 1:
                  placeholder = 'SteamID64/Url do perfil'
                  break
                case 2:
                  placeholder = 'PlaystationID'
                  break
                case 3:
                  placeholder = 'Gamertag'
                  break
              }
              return `Diga-me um(a) ${placeholder} válido(a)`
            }
          }
        },
        {
          id: 'season',
          prefix: 's:',
          match: 'prefix',
          prompt: {
            optional: true
          },
          type: (word, message) => {
            if (word >= 4 && word <= 7) return word
            return null
          }
        },
        {
          id: 'playlist',
          prefix: 'pl:',
          prompt: {
            optional: true
          },
          match: 'prefix',
          type: (word, message) => {
            switch (word) {
              case 'duel':
              case '1v1':
                return {type: 10, name: '1v1'}
              case 'doubles':
              case '2v2':
                return {type: 11, name: '2v2'}
              case 'solo standard':
              case 'ss':
              case '3v3s':
                return {type: 12, name: '3v3 Solo'}
              case 'standard':
              case '3v3':
                return {type: 13, name: '3v3'}
              default:
                return null
            }
          }
        }
      ]
    })
  }

  resolveEmoji (input) {
    const rank = [
      '<:Unranked:357949973725315072>', '<:Bronze1:357949974195077121>', '<:Bronze2:357949956134404097>', '<:Bronze3:357949955576430592>',
      '<:Silver1:357949975147184140>', '<:Silver2:357949969262706689>', '<:Silver3:357949967559688193>', '<:Gold1:357949960383234060>',
      '<:Gold2:357949958592135178>', '<:Gold3:357949975365287941>', '<:Platinum1:357949970223071234>', '<:Platinum2:357949955089891338>',
      '<:Platinum3:357949965672120320>', '<:Diamond1:357949964430606346>', '<:Diamond2:357949962090315787>', '<:Diamond3:357949959540047874>',
      '<:Champion1:357949968918511626>', '<:Champion2:357949971003211777>', '<:Champion3:357949961146597377>', '<:GrandChampion:357949957497421824>'
    ][input.tier]
    return rank
  }

  resolveDivision (input) {
    const division = ['I', 'II', 'III', 'IV', 'V'][input.division]
    return division
  }

  exec (message, { platform, season, playlist, player }) {
    let embed = new MessageEmbed()
      .setColor(`${message.member.displayHexColor === '#000000' ? message.guild.me.displayHexColor : message.member.displayHexColor}`)
      .setImage(`https://cdn.discordapp.com/attachments/256186350816985089/413046369385840640/rls_partner_horizontal_small.png`)
    request.get(`https://api.rocketleaguestats.com/v1/player`)
      .set('Authorization', process.env.rlstatsKey)
      .query('unique_id', player)
      .query('platform_id', platform)
      .then((r) => {
        r = r.body
        season = season || 7
        if (Object.keys(r.rankedSeasons).length === 0 || !r.rankedSeasons[season]) {
          embed
            .setDescription(`**Não foram encontrado ranks para o jogador ${r.displayName}**\n${season ? `Tem certeza que esse jogador jogou na ${season}ª temporada?` : 'Você tem certeza que esta pessoa joga Rocket League ranqueado?'}`)
            .setTimestamp()
          return message.channel.send({ embed })
        } else {
          embed
            .setAuthor(r.displayName, r.avatar)
            .addField('Estatísticas Completas', r.profileUrl)
            .setFooter(`Mostrando ranks para Temporada ${season || 7}`)
        }
        r = r.rankedSeasons[season]
        if (playlist) {
          embed
            .addField(`${this.resolveEmoji(r[playlist.type])} ${playlist.name}`, `**MMR:** ${r[playlist.type].rankPoints}${r[playlist.type].tier !== 0 ? `\n**Divisão:** ${this.resolveDivision(r[playlist.type])}` : ''}`)
        } else {
          if (r[10]) embed.addField(`${this.resolveEmoji(r[10])} 1v1`, `**MMR:** ${r[10].rankPoints}${r[10].tier !== 0 ? `\n**Divisão:** ${this.resolveDivision(r[10])}` : ''}`, true)
          if (r[11]) embed.addField(`${this.resolveEmoji(r[11])} 2v2`, `**MMR:** ${r[11].rankPoints}${r[11].tier !== 0 ? `\n**Divisão:** ${this.resolveDivision(r[11])}` : ''}`, true)
          if (r[10] || r[11]) embed.addBlankField()
          if (r[12]) embed.addField(`${this.resolveEmoji(r[12])} 3v3 Solo`, `**MMR:** ${r[12].rankPoints}${r[12].tier !== 0 ? `\n**Divisão:** ${this.resolveDivision(r[12])}` : ''}`, true)
          if (r[13]) embed.addField(`${this.resolveEmoji(r[13])} 3v3`, `**MMR:** ${r[13].rankPoints}${r[13].tier !== 0 ? `\n**Divisão:** ${this.resolveDivision(r[13])}` : ''}`, true)
        }
        embed
          .setTimestamp()
        return message.channel.send({ embed })
      })
      .catch((e) => {
        embed
          .setDescription(`**Jogador ${platform !== 1 ? player : `ID ${player}`} não foi encontrado**\nVocê tem certeza que esta pessoa joga Rocket League?`)
          .setTimestamp()

        return message.channel.send({ embed })
      })
  }
}

module.exports = RocketLeagueRankCommand
