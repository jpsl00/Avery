const { Command } = require('discord-akairo')
const request = require('snekfetch')
const Log = require('../../../util/Log')
const { MessageEmbed } = require('discord.js')

class RocketLeagueStatsCommand extends Command {
  constructor () {
    super('Rocket League Stats', {
      aliases: ['rlstats', 'rls'],
      description: 'Ver as estatísticas de um jogador no Rocket League',
      typing: true,
      cooldown: 30000,
      clientPermissions: ['EMBED_LINKS'],
      args: [
        {
          id: 'platform',
          description: 'A plataforma, pode ser `Steam, PS4 ou Xbox`',
          match: 'word',
          type: (word, message) => {
            switch (word.toLowerCase()) {
              case 'computador':
              case 'steam':
              case 'pc':
                return 1
              case 'playsation 4':
              case 'playstation':
              case 'ps4':
              case 'ps':
                return 2
              case 'xbox':
              case 'xbox one':
              case 'xb1':
              case 'x1':
                return 3
              default:
                return null
            }
          },
          prompt: {
            start: 'Diga-me a plataforma desejada\n*Steam, Playstation ou Xbox*',
            retry: 'Diga-me uma plataforma válida\n*Steam, Playstation ou Xbox*'
          }
        },
        {
          id: 'player',
          description: 'O jogador, pode ser `Gamertag, SteamID64 ou URL da Steam`',
          match: 'rest',
          type: (word, message, { platform }) => {
            if (!word) { return null }
            if (platform === 2 || platform === 3) {
              word = word.length ? word : null
            }
            if (platform === 1) {
              if (/(https?:\/\/steamcommunity\.com(\/(id)?(profiles)?\/[A-Za-z0-9]*))/.test(word)) {
                word = word
                  .replace(/(https?:\/\/steamcommunity\.com(\/(id)?(profiles)?\/*))/, '')
                  .replace('/', '')
              }
              if (!/^\d+$/.test(word) || word.length !== 17) {
                return request.get(`http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/`)
                  .query('key', process.env.steamKey)
                  .query('vanityurl', word)
                  .then((r) => {
                    r = r.body.response
                    if (r.success === 42) {
                      Log.info(r.message || 'Sem mensagem', {tag: 'SteamAPIVanityParser'})
                      return null
                    }
                    return r.steamid
                  })
                  .catch((e) => {
                    Log.info(e || 'Sem mensagem', {tag: 'SteamAPIRequest'})
                    return null
                  })
              }
            }
            return word
          },
          prompt: {
            start: (message, { platform }) => {
              let placeholder
              switch (platform) {
                case 1:
                  placeholder = 'SteamID64/Url do perfil'
                  break
                case 2:
                  placeholder = 'PlaystationID'
                  break
                case 3:
                  placeholder = 'Gamertag'
                  break
              }
              return `Diga-me um(a) ${placeholder}`
            },
            retry: (message, { platform }) => {
              let placeholder
              switch (platform) {
                case 1:
                  placeholder = 'SteamID64/Url do perfil'
                  break
                case 2:
                  placeholder = 'PlaystationID'
                  break
                case 3:
                  placeholder = 'Gamertag'
                  break
              }
              return `Diga-me um(a) ${placeholder} válido(a)`
            }
          }
        }
      ]
    })
  }

  exec (message, { platform, player }) {
    let embed = new MessageEmbed()
      .setColor(`${message.member.displayHexColor === '#000000' ? message.guild.me.displayHexColor : message.member.displayHexColor}`)
      .setImage(`https://cdn.discordapp.com/attachments/256186350816985089/413046369385840640/rls_partner_horizontal_small.png`)
    request.get(`https://api.rocketleaguestats.com/v1/player`)
      .set('Authorization', process.env.rlstatsKey)
      .query('unique_id', player)
      .query('platform_id', platform)
      .then((r) => {
        r = r.body
        embed
          .setAuthor(r.displayName, r.avatar)
          .addField('Estatísticas Completas', r.profileUrl)
          .addField('<:Wins:358269540540481537> Vitórias', r.stats.wins, true)
          .addField('<:Shots:358269539844227072> Chutes', r.stats.shots, true)
          .addField('<:Goals:358269541421416448> Gols', r.stats.goals, true)
          .addField('<:MVPs:358269540737613834> MVPs', r.stats.mvps, true)
          .addField('<:Assists:358269540284760064> Assistências', r.stats.assists, true)
          .addField('<:Saves:358269540456595456> Defesas', r.stats.saves, true)
          .setTimestamp()

        return message.channel.send({ embed })
      })
      .catch((e) => {
        embed
          .setDescription(`**Jogador ${platform !== 1 ? player : `ID ${player}`} não foi encontrado**\nVocê tem certeza que esta pessoa joga Rocket League?`)
          .setTimestamp()

        return message.channel.send({ embed })
      })
  }
}

module.exports = RocketLeagueStatsCommand
