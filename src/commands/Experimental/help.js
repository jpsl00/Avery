const CommonUtil = require('../../util/CommonUtil')

const { stripIndents } = require('common-tags')
const { MessageEmbed } = require('discord.js')
const { Command } = require('discord-akairo')

class HelpCommand extends Command {
  constructor () {
    super('help', {
      aliases: ['help', 'h'],
      typing: true,
      description: 'Ver a lista de comandos ou detalhes de um comando específico.',
      args: [
        {
          id: 'info',
          type: 'string',
          match: 'content',
          prompt: {
            optional: true
          },
          default: null,
          description: 'Pode ser uma Categoria, um Comando ou nada'
        }
      ]
    })
  }

  _getFullList (message) {
    const prefix = this.handler.prefix(message)
    const embed = new MessageEmbed()
      .setColor(`${message.guild ? message.member.displayHexColor === '#000000' ? message.guild.me.displayHexColor : message.member.displayHexColor : '#ff5722'}`)
      .setFooter(`${message.author.username}`, `${message.author.displayAvatarURL()}`)

    this.handler.categories.forEach((value, key) => {
      let content = []

      value.forEach((com) => {
        if ((com.hidden || com.ownerOnly) && !this.client.ownerID.includes(message.author.id)) return
        content.push(`\`${CommonUtil.emoji.enabled[com.enabled]} ${prefix}${com.aliases[0]}\` ${com.description || ''}`)
      })

      if (!content.length) return
      embed
        .addField(`${key.toTitleCaseAll()}`, `${content.join(`\n`)}`, true)
    })

    return embed
  }

  _getCmdList (message, category, name) {
    const prefix = this.handler.prefix(message)
    const embed = new MessageEmbed()
      .setTitle(`${name.toTitleCase()}`)
      .setColor(`${message.guild ? message.member.displayHexColor === '#000000' ? message.guild.me.displayHexColor : message.member.displayHexColor : '#ff5722'}`)
      .setFooter(`${message.author.username}`, `${message.author.displayAvatarURL()}`)

    category.forEach((value, key) => {
      if ((value.hidden || value.ownerOnly) && !this.client.ownerID.includes(message.author.id)) return
      let content = []

      content
        .push(`\`${CommonUtil.emoji.enabled[value.enabled]}\` ${CommonUtil.text.enabled[value.enabled]}`)
      content
        .push(`${value.aliases.length > 1 ? `\`${prefix}${value.aliases.join(`, ${prefix}`)}\`` : ``}`)
      content
        .push(`${value.description}`)

      if (!content.length) return
      embed
        .addField(`${prefix}${key}`, `${content.join(`\n`)}`, true)
    })
    return embed
  }

  _getCmdInfo (message, command) {
    const prefix = this.handler.prefix(message)
    const embed = new MessageEmbed()
      .setTitle(`${command.id.toTitleCaseAll()}`)
      .setDescription(stripIndents`
        \`${CommonUtil.emoji.enabled[command.enabled]}\` ${CommonUtil.text.enabled[command.enabled]}
        ${command.aliases.length > 1 ? `\`${prefix}${command.aliases.join(`, ${prefix}`)}\`\n` : ``}${command.description}`)
      .setColor(`${message.guild ? message.member.displayHexColor === '#000000' ? message.guild.me.displayHexColor : message.member.displayHexColor : '#ff5722'}`)
      .setFooter(`${message.author.username}`, `${message.author.displayAvatarURL()}`)

    command.args.forEach((value) => {
      let optional = value.prompt ? value.prompt.optional : false
      embed
        .addField(stripIndents`
          \`${value.id}\`
        `, stripIndents`
          ${optional ? '**Opcional**' : '**Obrigatório**'}
          ${value.prefix ? `Prefixo: \`${value.prefix}\`\n` : ``}${value.description || ``}
        `, true)
    })

    return embed
  }

  exec (message, { info }) {
    const resolver = this.handler.resolver.type('commandAlias')
    if (info) {
      // Find command or category
      if (this.handler.categories.has(info)) { // Found a category
        const category = this.handler.categories.get(info)
        message.author.send({ embed: this._getCmdList(message, category, info) })
      } else if (resolver(info)) { // Found a command
        const command = resolver(info)
        message.author.send({ embed: this._getCmdInfo(message, command) })
      } else {
        return message.util.replyEmbed(`Não encontrei nenhum Comando ou Categoria com o nome \`${info}\``)
      }
      return message.util.sendGuild(`<:envelopeWithArrow:423885600081575936> Enviei no seu privado!`)
    }
    // List all categories if none was provided
    message.author.send({ embed: this._getFullList(message) })
    return message.util.sendGuild(`<:envelopeWithArrow:423885600081575936> Enviei no seu privado!`)
  }
}

module.exports = HelpCommand
